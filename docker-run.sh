#!/usr/bin/env bash
docker run -d --net=host \
  -e HASURA_GRAPHQL_DATABASE_URL=postgres://postgres:password0103@localhost:5432/hasura_saas \
  -e HASURA_GRAPHQL_ENABLE_CONSOLE=true \
  -e HASURA_GRAPHQL_ACCESS_KEY="${HASURA_SAAS_ACCESS_KEY}" \
  -e HASURA_GRAPHQL_AUTH_HOOK=localhost:8000/api/v1/users/webhook/auth/ \
  hasura/graphql-engine:latest